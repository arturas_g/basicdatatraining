﻿using System.Collections.Generic;
using System.Data;

namespace BasicDataTraining
{
    /*
     *  TODO: 1. Add input validation and null checks
     *        2. Add exception / error handling
     *        3. Implement logging
     *        4. Dynamic mapping through custom attributes or Automapper or EntityFramework
     *        5. Separate projects: main class and model in one, all utility classes in another
     */
    class Program
    {
        private const string inputFilePath = @"D:\Google Drive\projects\c#\BasicDataTraining\BasicDataTraining\input\Case 1.xlsx";
        private const string outputFilePath = @"D:\Google Drive\projects\c#\BasicDataTraining\BasicDataTraining\output\Case 1.xml";
        private const string backupFilePath = @"D:\Google Drive\projects\c#\BasicDataTraining\BasicDataTraining\backup\Case 1.xml";
        private const string restoreFilePath = @"D:\Google Drive\projects\c#\BasicDataTraining\BasicDataTraining\output\Restore Case 1.xlsx";

        static void Main(string[] args)
        {
            IFileReader fileReader = new ExcelFileReader(inputFilePath);
            DataSet dataSet = fileReader.ReadData();

            // save xml before mapping because we are not mapping all excel fields
            // if we want to serialize model after mapping we need backward mapping to restore to excel
            IFileWriter fileWriter = new XmlFileWriter();
            fileWriter.WriteData(dataSet.GetXml(), outputFilePath, backupFilePath);

            Mapper mapper = new Mapper();
            // convert to cuncurrency collection for thread safety if needed
            List<Model.Customer> customerList = mapper.Map(dataSet);
            
            BackupHandler backupHandler = new BackupHandler();
            backupHandler.RestoreExcelFile(backupFilePath, restoreFilePath);
        }
        
    }
}
