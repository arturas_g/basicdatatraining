﻿using System;

namespace BasicDataTraining
{
    /// <summary>
    /// Custom attribute that stores table column name that should be mapped to property
    /// </summary>
    [AttributeUsage(AttributeTargets.Property)]
    class ColumnNameAttribute : Attribute
    {
        private string columnName;

        public ColumnNameAttribute(params string[] columnName)
        {
            this.columnName = columnName[0];
        }

        public string ColumnName => columnName;
        
    }
}
