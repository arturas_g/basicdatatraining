﻿using System.Xml;

namespace BasicDataTraining
{
    /// <summary>
    /// Class responsible for writing data to XML file.
    /// </summary>
    class XmlFileWriter : IFileWriter
    {
        /// <summary>
        /// Write data to XML file.
        /// </summary>
        /// <param name="data">Data to write.</param>
        /// <param name="filePath">Path to XML file.</param>
        /// <param name="backupFilePath">Path to backup XML file.</param>
        public void WriteData(string data, string filePath, string backupFilePath)
        {
            XmlDocument xdoc = new XmlDocument();
            xdoc.LoadXml(data);
            xdoc.Save(filePath);
            xdoc.Save(backupFilePath);
        }
    }
}
