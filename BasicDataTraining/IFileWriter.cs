﻿namespace BasicDataTraining
{
    public interface IFileWriter
    {
        void WriteData(string data, string filePath, string backupFilePath);
    }
}
