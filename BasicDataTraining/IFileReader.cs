﻿using System.Data;

namespace BasicDataTraining
{
    public interface IFileReader
    {
        DataSet ReadData();
        DataSet ReadData(string filePath);
    }
}
