﻿using System.Data;
using System.Data.OleDb;

namespace BasicDataTraining
{
    /// <summary>
    /// Class responsible for reading data from excel file.
    /// </summary>
    class ExcelFileReader : IFileReader
    {
        private string FilePath { get; set; }

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="filePath">Path to excel file.</param>
        public ExcelFileReader(string filePath)
        {
            FilePath = filePath;
        }

        /// <summary>
        /// Read data from excel file that was used in constructor.
        /// </summary>
        /// <returns>DataSet containing data from excel file.</returns>
        public DataSet ReadData()
        {
            return ReadData(FilePath);
        }

        /// <summary>
        /// Read data from provided file.
        /// </summary>
        /// <param name="filePath">Path to excel file.</param>
        /// <returns>DataSet containing data from excel file.</returns>
        public DataSet ReadData(string filePath)
        {
            DataSet dataSet = new DataSet();
            string strConn = string.Format("Provider=Microsoft.ACE.OLEDB.12.0;Data Source={0};Extended Properties=\"Excel 12.0 Xml;HDR=YES;IMEX=1;TypeGuessRows=0;ImportMixedTypes=Text\"", filePath);
            using (OleDbConnection dbConnection = new OleDbConnection(strConn))
            {
                dbConnection.Open();
                DataTable objSheetNames = dbConnection.GetSchema("Tables");

                foreach (DataRow row in objSheetNames.Rows)
                {
                    string sheetName = row["TABLE_NAME"].ToString();
                    using (OleDbDataAdapter dbAdapter = new OleDbDataAdapter($"SELECT * FROM [{sheetName}]", dbConnection))
                        dbAdapter.Fill(dataSet, sheetName.Remove(sheetName.Length - 1));
                }

                return dataSet;
            }
        }
    }
}
