﻿using Microsoft.Office.Interop.Excel;
using System;
using System.Data;

namespace BasicDataTraining
{
    /// <summary>
    /// Class responsible for backup operations
    /// </summary>
    class BackupHandler
    {
        /// <summary>
        /// Restore excel file from xml file
        /// </summary>
        /// <param name="backupFilePath">Path to backup xml file</param>
        /// <param name="restoreFilePath">Path to restored file</param>
        public void RestoreExcelFile(string backupFilePath, string restoreFilePath)
        {
            DataSet dataSet = new DataSet();
            dataSet.ReadXml(backupFilePath);

            Application excel = new Application();
            excel.Visible = false;
            excel.DisplayAlerts = false;
            Workbook book = excel.Application.Workbooks.Add(Type.Missing);

            foreach (System.Data.DataTable dt in dataSet.Tables)
            {
                Worksheet excelWorkSheet = (Worksheet)book.ActiveSheet;
                excelWorkSheet.Name = dt.TableName;

                // Header
                for (int i = 1; i < dt.Columns.Count + 1; i++)
                {
                    excelWorkSheet.Cells[1, i] = dt.Columns[i - 1].ColumnName;
                }

                //Data
                for (int j = 0; j < dt.Rows.Count; j++)
                {
                    for (int k = 0; k < dt.Columns.Count; k++)
                    {
                        excelWorkSheet.Cells[j + 2, k + 1] = dt.Rows[j].ItemArray[k].ToString();
                    }
                }

                if (book.Worksheets.Count < dataSet.Tables.Count)
                {
                    book.Worksheets.Add();
                }
            }

            book.SaveAs(restoreFilePath);
            book.Close(true);
            excel.Quit();
        }
    }
}
