﻿using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace BasicDataTraining
{
    /// <summary>
    /// Mapper class responsible for mapping dataset to model.
    /// </summary>
    public class Mapper
    {
        private const string CustomerTableName = @"Kunder";
        private const string AddressTableName = @"Adresser";
        private const string ContactTableName = @"Kontaktpersoner";

        /// <summary>
        /// Map dataset to model.
        /// </summary>
        /// <param name="dataSet">Datset to map from.</param>
        /// <returns>List of models.</returns>
        public List<Model.Customer> Map(DataSet dataSet)
        {
            List<Model.Customer> customerList = new List<Model.Customer>();

            DataTable rootTable = GetDataTableByName(dataSet, CustomerTableName);

            // TODO: use custom attribute to map column name with property
            //var a = typeof(Model.Customer).GetCustomAttributes(typeof(ColumnNameAttribute), true);
            //var a = typeof(Model.Customer).GetProperties();


            // Simple implementation to test solution.
            // Should be converted to dynamic mapping through usage of custom attributes
            // Consider using Automapper or EF
            foreach (DataRow row in rootTable.Rows)
            {
                customerList.Add(new Model.Customer()
                {
                    CustomerNo = row["Kundenummer"].ToString(),
                    CustomerName = row["Kundenavn"].ToString(),
                    Addresses = new List<Model.Address>(),
                    Contacts = new List<Model.Contact>()
                });

            }

            foreach (DataTable table in dataSet.Tables)
            {
                if (table.TableName == CustomerTableName)
                {
                    continue;
                }

                switch (table.TableName)
                {
                    case AddressTableName:
                        foreach (DataRow row in table.Rows)
                        {
                            Model.Customer customer = GetCustomer(customerList, row);
                            customer.Addresses.Add(new Model.Address()
                            {
                                Address1 = row["Adresse1"].ToString(),
                                Address2 = "",
                                PostalArea = row["Poststed"].ToString(),
                                PostalCode = row["Postnummer"].ToString()

                            });
                        }
                        break;
                    case ContactTableName:
                        foreach (DataRow row in table.Rows)
                        {
                            Model.Customer customer = GetCustomer(customerList, row);
                            customer.Contacts.Add(new Model.Contact()
                            {
                                ContactName = row["Navn"].ToString(),
                                Telephone = row["Telefon"].ToString(),
                                Email = row["Email"].ToString()

                            });
                        }
                        break;
                    default:
                        break;
                }

            }

            return customerList;
        }

        private static Model.Customer GetCustomer(List<Model.Customer> customerList, DataRow row)
        {
            string id = row["Kundenummer"].ToString();
            Model.Customer customer = customerList.Where(c => c.CustomerNo.Equals(id)).FirstOrDefault();
            return customer;
        }

        private DataTable GetDataTableByName(DataSet dataSet, string tableName)
        {
            foreach (DataTable table in dataSet.Tables)
            {
                if (table.TableName == tableName)
                {
                    return table;
                }
            }
            return null;
        }
    }
}
