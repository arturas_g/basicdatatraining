﻿using System.Collections.Generic;

namespace BasicDataTraining
{
    public class Model
    {
        public class Customer
        {
            [ColumnName("Kundenummer")]
            public string CustomerNo { get; set; }
            public string CustomerName { get; set; }
            public IList<Address> Addresses { get; set; }
            public IList<Contact> Contacts { get; set; }
        }

        public class Address
        {
            public string Address1 { get; set; }
            public string Address2 { get; set; }
            public string PostalArea { get; set; }
            public string PostalCode { get; set; }
        }

        public class Contact
        {
            public string ContactName { get; set; }
            public string Email { get; set; }
            public string Telephone { get; set; }
        }
    }
}
